import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone
def getTimestamp() {
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"))
    def sdf = new SimpleDateFormat("yyyyMMddHHmmss")
    return "crc_v${sdf.format(new Date())}"
} 
pipeline {
    options{
      timeout(time: 3, unit: 'HOURS')
    }
    agent any
    parameters {
        choice(
         name: 'APP_NAME',
	           choices: ['aios-manage','aios-web','bi-api','bi-web','bpmc-one-api','bpmc-portal-api','bpmc-web','card-api','card-web','cdp-api','cdp-manage','cdp-web','cdp-worker','centos-for-dev','ema-manage',
             'ema-web','gmp-manage','gmp-proxy','gmp-web','lingxi-backend-api','lingxi-web'],
	           description: '选择需要发布更新的服务'
        )
        choice(
         name: 'APP_SCM',
	           choices: ['http://gerrit.reconova.com:8081/a/cdp/cdp-manage','http://gerrit.reconova.com:8081/a/cdp/cdp-worker','http://gerrit.reconova.com:8081/a/cdp/cdp-api',
             'http://gerrit.reconova.com:8081/a/bpmc/bpmc-backend-api','http://gerrit.reconova.com:8081/a/bpmc/bpmc-backend-one-api','http://gerrit.reconova.com:8081/a/ema/ema-manage',
             'http://gerrit.reconova.com:8081/a/lingxi/lingxi-backend-api','http://gerrit.reconova.com:8081/a/card/card-manage','http://gerrit.reconova.com:8081/a/bi/bi-manage',
             'http://gerrit.reconova.com:8081/a/aios/aios-manage','http://lijian@gerrit.reconova.com:8081/a/GMP/gmp-manager','http://gerrit.reconova.com:8081/a/GMP/gmp-proxy',
             ' http://gerrit.reconova.com:8081/a/ema/ema-web','http://gerrit.reconova.com:8081/a/card/card-web','http://gerrit.reconova.com:8081/a/lingxi/lingxi-frontend','http://gerrit.reconova.com:8081/a/cdp/cdp-web','http://gerrit.reconova.com:8081/a/aios/aios-web','http://gerrit.reconova.com:8081/a/bpmc/bpmc-web'],
	           description: '选择服务的代码仓库地址'
        )
        choice(
         name: 'BRANCHES_NAME',
	           choices: ['master','test','release'],
	           description: '选择服务的代码仓库分支'
        )
        string(name:'TAG_NAME',defaultValue: getTimestamp(),description:'镜像名称:默认crc_v+当前日期格式(yyyyMMddHHmmssUTC时间),必须满足crc_v* 格式才能触发发布操作')                  
        string(name:'REMARKS',defaultValue: '',description:'更新发布说明:字数不要太多;必填项否则不执行')
    }

    environment {
        DOCKER_CREDENTIAL_ID = 'alibaba-registry'
        GERRIT_CREDENTIAL_ID='gerrit'
        GITHUB_CREDENTIAL_ID = 'gitlab'
        KUBECONFIG_CREDENTIAL_ID = 'cr-kubeconfig'
        REGISTRY = 'registry.cn-zhangjiakou.aliyuncs.com'
        DOCKERHUB_NAMESPACE = 'reconova'
        GERRIT_ACCOUNT = 'lijian'
        DESCRIPTION = "${params.REMARKS}"
        BUILD_TRIGGER_BY = "${currentBuild.getBuildCauses()[0].userId}"
    }

    stages {
      stage('parallel to executor build'){
        parallel {
          stage ('Checkout and build and push image with tag for jdk8') {
          when{
            expression{
              return !params.APP_NAME.matches('.*-web') && !params.APP_NAME.matches('card-api') && !params.APP_NAME.matches('bi-api') && params.REMARKS != ''
            }
          }
          agent {
            label 'maven'
          }          
            steps {
                script {
                    checkout([$class: 'GitSCM',branches: [[name: '*/$BRANCHES_NAME']],doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: env.GERRIT_CREDENTIAL_ID, url: params.APP_SCM]]])
                }
                container ('maven') {
                    echo "[++]start  build this project[++]"
                    sh 'mvn clean package -DskipTests=true -Ppro'
                    sh 'cp -rf target/*.jar docker/'
                    sh 'cd docker && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
          }
          stage ('Checkout and build and push image with tag for jdk15') {
          when{
            expression{
              return params.REMARKS != '' && (params.APP_NAME.matches('card-api') || params.APP_NAME.matches('bi-api'))
            }
          }
          agent {
            label 'jdk15'
          }          
            steps {
                script {
                    checkout([$class: 'GitSCM',branches: [[name: '*/$BRANCHES_NAME']],doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: env.GERRIT_CREDENTIAL_ID, url: params.APP_SCM]]])
                }
                container ('maven') {
                    echo "[++]start  build this project[++]"
                    sh 'mvn clean package -DskipTests=true -Ppro'
                    sh 'cp -rf target/*.jar docker/'
                    sh 'cd docker && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
          }
          stage ('Checkout and build and push image with tag for gradle') {
          when{
            expression{
              return params.APP_NAME=='gmp-proxy'
            }
          }
          agent {
            label 'gradle'
          }          
            steps {
                script {
                    checkout([$class: 'GitSCM',branches: [[name: '*/$BRANCHES_NAME']],doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: env.GERRIT_CREDENTIAL_ID, url: params.APP_SCM]]])
                }
                container ('maven') {
                    echo "[++]start  build this project[++]"
                    sh 'gradle build -x test -x bootStartScripts -x bootDistTar -x bootDistZip -x distTar -x distZip'
                    sh 'cp  build/libs/*.jar    docker/'
                    sh 'cd docker && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
          }
        stage ('Checkout and package and push image with tag for nodejs') {
          when{
            expression{
              return params.APP_NAME=~/.*-web/ && params.REMARKS != ''
            }
          }
          agent {
            kubernetes {
              inheritFrom 'nodejs base'
              containerTemplate {
              name 'nodejs'
              image 'node:14.19.0'
              }
            }
          }          
            steps {
                script {
                    checkout([$class: 'GitSCM',branches: [[name: '*/$BRANCHES_NAME']],doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    submoduleCfg: [],
                    userRemoteConfigs: [[credentialsId: env.GERRIT_CREDENTIAL_ID, url: params.APP_SCM]]])
                }
                container ('nodejs'){
                    echo "[++]start  build this project[++]"
                    sh 'npm config set registry https://registry.npmmirror.com/ && npm config set ELECTRON_MIRROR https://npmmirror.com/mirrors/electron/'
                    sh 'npm install --unsafe-perm && rm -rf dist'
                    sh 'npm run build'
                    sh 'cp -rf dist docker/'
                    sh 'cd docker/ && docker build  -t $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER .'
                    echo "[++]start push image with tag[++]"
                    withCredentials([usernamePassword(passwordVariable: 'DOCKER_PASSWORD',usernameVariable: 'DOCKER_USERNAME',credentialsId: "$DOCKER_CREDENTIAL_ID",)]){
                    sh 'echo "$DOCKER_PASSWORD" | docker login $REGISTRY -u "$DOCKER_USERNAME" --password-stdin'
                    sh 'docker tag  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '
                    sh 'docker push  $REGISTRY/$DOCKERHUB_NAMESPACE/$APP_NAME:$TAG_NAME '

              }
                }
            }
        }
        }

      }
      stage("parallel to execute deploy"){
        parallel{
          stage('deploy for java server'){
          when{
            expression{
              return params.TAG_NAME =~ /crc_v.*/ && !params.APP_NAME.matches('.*-web') && params.REMARKS != ''
            }
          }
          agent {
            label 'maven'
          }          
          steps {
              echo "[++]send message to admin [++]"
              container('maven'){
                sh "curl 'https://oapi.dingtalk.com/robot/send?access_token=08203a279296870d5b5f28959c969ec7481903eef3b3366bb9cbf82063eb8285' -H 'Content-Type: application/json' -d '{\"msgtype\": \"markdown\",\"at\":{\"atMobiles\":[\"18720934668\"],\"isAtAll\": true},\"markdown\": {\"title\": \"[星汉平台]欢乐颂项目CI/CD构建通知\",\"text\":\"**欢乐颂项目CI/CD 审核通知**\n\n  **构建项目:** <font color=#FF0000><b>$APP_NAME</b></font>\n\n --------------------------------------\n\n----------------------------------\n\n构建用户: $BUILD_TRIGGER_BY \n\n ----------------------------------\n\n 容器镜像Tag: $TAG_NAME \n\n -----------------------------------\n\n更新内容: $REMARKS \n\n\"}}'"
              }

              input(id: 'deploy-to-production', message: "[更新]:${env.DESCRIPTION}",submitter: 'admin')
              container ('maven') {
              echo "deploy to production"
              withCredentials([
                    kubeconfigFile(
                    credentialsId: env.KUBECONFIG_CREDENTIAL_ID,
                    variable: 'KUBECONFIG')
                    ]) {
                    sh 'kubectl  --insecure-skip-tls-verify set image deployment/${APP_NAME} ${APP_NAME}=${REGISTRY}/${DOCKERHUB_NAMESPACE}/${APP_NAME}:${TAG_NAME} -n xinghan'
              }  
              }
          }
        }
        stage('deploy for web'){
          when{
            expression{
              return params.TAG_NAME =~ /crc_v.*/ &&  params.APP_NAME.matches('.*-web') && params.REMARKS != ''
            }
          }
          agent {
            kubernetes {
              inheritFrom 'nodejs base'
              containerTemplate {
              name 'nodejs'
              image 'node:14.19.0'
              }
            }
          }          
          steps {
              echo "[++]send message to admin [++]"
              container('nodejs'){
              sh "curl 'https://oapi.dingtalk.com/robot/send?access_token=08203a279296870d5b5f28959c969ec7481903eef3b3366bb9cbf82063eb8285' -H 'Content-Type: application/json' -d '{\"msgtype\": \"markdown\",\"at\":{\"atMobiles\":[\"18720934668\"],\"isAtAll\": true},\"markdown\": {\"title\": \"[星汉平台]欢乐颂项目CI/CD构建通知\",\"text\":\"**欢乐颂项目CI/CD 审核通知**\n\n  **构建项目:** <font color=#FF0000><b>$APP_NAME</b></font>\n\n --------------------------------------\n\n----------------------------------\n\n构建用户: $BUILD_TRIGGER_BY \n\n ----------------------------------\n\n 容器镜像Tag: $TAG_NAME \n\n -----------------------------------\n\n更新内容: $REMARKS \n\n\"}}'"
              }
              input(id: 'deploy-to-production', message: "[更新]:${env.DESCRIPTION}",submitter: 'admin')
              container ('nodejs') {
              echo "deploy to production"
              withCredentials([
                    kubeconfigFile(
                    credentialsId: env.KUBECONFIG_CREDENTIAL_ID,
                    variable: 'KUBECONFIG')
                    ]) {
                    sh 'kubectl --insecure-skip-tls-verify set image deployment/${APP_NAME} ${APP_NAME}=${REGISTRY}/${DOCKERHUB_NAMESPACE}/${APP_NAME}:${TAG_NAME} -n xinghan'
                    }  
              }
          }
        }

        }
      }        
    }               
    post {
      always{
        mail to: "xinghan@reconova.com",
        cc: "lijian@reconova.com",
        subject: "Jenkins Pipeline: ${currentBuild.fullDisplayName} build ${currentBuild.currentResult}通知",
        body: "<b>[构建信息]</b></br><b>构建结果 - ${currentBuild.currentResult}</b></br><hr size='2' width='100%' align='center'/>项目名称：${currentBuild.fullProjectName}</br>构建编号 ： 第${env.BUILD_NUMBER}次构建</br>触发原因： ${env.CAUSE}</br>构建日志： <a href='${env.BUILD_URL}console'>${env.BUILD_URL}console</a></br> 工作目录 ： <a href='${env.PROJECT_URL}ws'>${env.PROJECT_URL}ws</a></br>项目 Url ： <a href='${currentBuild.absoluteUrl}'>${currentBuild.absoluteUrl}</a></br>持续时长：${currentBuild.duration}ms</br>更新内容：${env.DESCRIPTION}</br><hr size='2' width='100%' align='center'/>[本邮件由系统自动发出，无需回复！]",mimeType: "text/html"
        echo "[++]send dingding message to ding group [++]"
        sh "curl 'https://oapi.dingtalk.com/robot/send?access_token=08203a279296870d5b5f28959c969ec7481903eef3b3366bb9cbf82063eb8285' -H 'Content-Type: application/json' -d '{\"msgtype\": \"markdown\",\"markdown\": {\"title\": \"[星汉平台]欢乐颂项目CI/CD构建通知\",\"text\":\"**欢乐颂项目CI/CD 构建结束通知**\n\n  **构建结果:** <font color=#FF0000><b>$currentBuild.currentResult</b></font>\n\n --------------------------------------\n\n构建项目: $APP_NAME \n\n ----------------------------------\n\n构建用户: $BUILD_TRIGGER_BY \n\n ----------------------------------\n\n  构建序号: $BUILD_DISPLAY_NAME \n\n ----------------------------------\n\n  容器镜像Tag: $TAG_NAME(tag名称需满足匹配 crc_v.*否则不会触发部署) \n\n -----------------------------------\n\n更新内容: $REMARKS \n\n\"}}'"
      }    
  }
}
